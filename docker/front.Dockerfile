FROM nginx:1.19.2-alpine

COPY ./docker/front-entrypoint.sh /front-entrypoint.sh
RUN chmod +x front-entrypoint.sh

WORKDIR /usr/share/nginx/html

COPY ./dist/apps/spa .

EXPOSE 80

ENTRYPOINT [ "/front-entrypoint.sh" ]

