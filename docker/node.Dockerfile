FROM node:13.14-alpine

WORKDIR /app

ARG APP_NAME
RUN : "${APP_NAME:?Missing required arg APP_NAME}"

COPY ./dist/apps/$APP_NAME .

USER node

EXPOSE 8080

ENTRYPOINT ["node", "main.js"]