import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { LinkShortenerModule } from './link-shortener/link-shortener.module'
import { SharedModule } from './shared/shared.module'
import { ConfigService } from './shared/config.service'
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

export function initConfig(configService: ConfigService) {
  return () => configService.loadConfig();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([], { initialNavigation: 'enabled' }),
    LinkShortenerModule,
    HttpClientModule,
    NgbModule,
    SharedModule
  ],
  providers: [{provide: APP_INITIALIZER, deps: [ConfigService], useFactory: initConfig, multi: true}],

  bootstrap: [AppComponent],
})
export class AppModule {}
