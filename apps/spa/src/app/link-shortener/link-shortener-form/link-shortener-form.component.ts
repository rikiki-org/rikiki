import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LinkShortenerService } from '../link-shortener.service';

@Component({
  selector: 'rikiki-link-shortener-form',
  templateUrl: './link-shortener-form.component.html',
  styleUrls: ['./link-shortener-form.component.css']
})
export class LinkShortenerFormComponent implements OnInit {

  constructor(private linkShortenerService: LinkShortenerService) { }

  @Output() linkShortened = new EventEmitter<any>();

  urlControl = new FormControl('');
  slugControl = new FormControl('');

  ngOnInit(): void {
  }

  shortenUrl(){

    const payload = {
      longUrl: this.urlControl.value,
      slug: this.slugControl.value ? this.slugControl.value : undefined
    }

    this.linkShortenerService.shortenUrl(payload).subscribe(res => this.linkShortened.emit(res));
    
  }
}
