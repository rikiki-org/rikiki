import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'rikiki-link-shortener-result',
  templateUrl: './link-shortener-result.component.html',
  styleUrls: ['./link-shortener-result.component.css']
})
export class LinkShortenerResultComponent implements OnInit {

  @Input() shortLinkResult;
  @Output() goBackEmitter = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  goBack() {
    this.goBackEmitter.emit();
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
  }

}
