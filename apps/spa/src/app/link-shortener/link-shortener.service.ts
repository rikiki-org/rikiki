import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IShortlinkCreateDto } from '@rikiki/models';
import { ConfigService } from '../shared/config.service';

@Injectable({
  providedIn: 'root'
})
export class LinkShortenerService {

  constructor(private http: HttpClient, private configService: ConfigService) { 
    this.apiUrl = this.configService.get("apiUrl")
  }

  apiUrl: string;

  shortenUrl(shortlink: IShortlinkCreateDto) {
    return this.http.post(this.apiUrl+'/v1/shortlinks', shortlink)
  }

}
