import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkShortenerComponent } from './link-shortener/link-shortener.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LinkShortenerFormComponent } from './link-shortener-form/link-shortener-form.component';
import { LinkShortenerResultComponent } from './link-shortener-result/link-shortener-result.component';



@NgModule({
  declarations: [LinkShortenerComponent, LinkShortenerFormComponent, LinkShortenerResultComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [LinkShortenerComponent],
})
export class LinkShortenerModule { }
