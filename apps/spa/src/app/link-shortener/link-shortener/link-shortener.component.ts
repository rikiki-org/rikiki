import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'rikiki-link-shortener',
  templateUrl: './link-shortener.component.html',
  styleUrls: ['./link-shortener.component.css']
})
export class LinkShortenerComponent implements OnInit {

  showForm = true;
  shortLinkResult;

  constructor() { }


  ngOnInit(): void {
  }

  onLinkShortened(shortlink) {
    this.shortLinkResult = shortlink;
    this.showForm = false;
  }

  onGoBack(){
    delete this.shortLinkResult;
    this.showForm = true;
  }

}
