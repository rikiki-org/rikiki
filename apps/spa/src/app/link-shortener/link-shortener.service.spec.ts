import { TestBed } from '@angular/core/testing';

import { LinkShortenerService } from './link-shortener.service';
import { HttpClient } from '@angular/common/http';

import { } from 'jest'

describe('LinkShortenerService', () => {
  let service: LinkShortenerService;

  let httpClientSpy;

  beforeEach(() => {

    httpClientSpy = () => ({
      post: jest.fn()
    })

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(LinkShortenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
