import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'rikiki-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'spa';

}
