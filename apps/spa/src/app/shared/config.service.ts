import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient) { }

  config: Map<string, string> = new Map()

  get(key: string){
    return this.config.get(key)
  }

  loadConfig(){
    return this.http.get("/assets/env/env.json").toPromise().then(
      data => {
        Object.getOwnPropertyNames(data).forEach(key => this.config.set(key, data[key]))
        console.log("Current config is:");
        console.log(this.config);
        
      }
    )
  }
}
