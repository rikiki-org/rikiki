import { TestBed } from '@angular/core/testing';

import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';

describe('ConfigService', () => {
  let service: ConfigService;
  
  let httpClientSpy;

  beforeEach(() => {

    httpClientSpy = () => ({
      get: jest.fn()
    })

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(ConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
