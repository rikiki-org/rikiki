import { Injectable, HttpService } from '@nestjs/common';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Shortlink } from '@rikiki/models';

@Injectable()
export class ShortlinksService {

    constructor(private httpService: HttpService) {}

    // TODO use shared config service
    apiUrl = process.env.API_URL || 'http://localhost:8080/api'

    async getBySlug(slug: string): Promise<false|Shortlink> {

        console.log('test');
        

        return this.httpService.get<Shortlink>(`${this.apiUrl}/v1/shortlinks/${slug}`).pipe(
            map(res => res.data),
            catchError(() => of(false))
        )
        .toPromise() as Promise<false|Shortlink>

    }

}
