import { Test, TestingModule } from '@nestjs/testing';
import { RedirectController } from './redirect.controller';
import { ShortlinksService } from './shortlinks.service';
import { HttpModule } from '@nestjs/common';

describe('RedirectController', () => {
  let controller: RedirectController;
  let shortlinksService: ShortlinksService

  const mockService = {getBySlug: jest.fn()}

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RedirectController],
      imports: [HttpModule],
      providers: [ShortlinksService],
    })
    .overrideProvider(ShortlinksService)
    .useValue(mockService)
    .compile();

    controller = module.get<RedirectController>(RedirectController);
    shortlinksService = module.get<ShortlinksService>(ShortlinksService)

  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('get with slug', () => {
    it('should redirect to url returned by shortlink service', async () => {

      const shortlink = {slug: 'test', longUrl: 'http://google.com'};
      jest.spyOn(shortlinksService, 'getBySlug').mockResolvedValue(shortlink);

      const result = { 
        statusCode: 301,
        url: shortlink.longUrl
      }

      expect(await controller.redirect('test')).toEqual(result)

    })

    it('should return nothing if not found',async () => {
      
      jest.spyOn(shortlinksService, 'getBySlug').mockResolvedValue(false);
      const result = undefined
      expect(await controller.redirect('test')).toEqual(result)

    })

  })


});
