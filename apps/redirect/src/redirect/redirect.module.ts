import { Module, HttpModule } from '@nestjs/common';
import { RedirectController } from './redirect.controller';
import { ShortlinksService } from './shortlinks.service';

@Module({
  imports: [HttpModule],
  controllers: [RedirectController],
  providers: [ShortlinksService]
})
export class RedirectModule {}
