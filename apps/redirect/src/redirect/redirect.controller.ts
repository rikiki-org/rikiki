import { Controller, Redirect, Param, Get } from '@nestjs/common';
import { ShortlinksService } from './shortlinks.service';

@Controller()
export class RedirectController {

    constructor(private shortlinksService: ShortlinksService) {}

    @Get(':slug**')
    @Redirect('../app/not-found', 404)
    async redirect(@Param('slug') slug: string): Promise<unknown> {

        const shortlink = await this.shortlinksService.getBySlug(slug);

        if (!shortlink) {
            return;
        }

        // Override default redirect see https://docs.nestjs.com/controllers#redirection
        return {
            url: shortlink.longUrl,
            statusCode: 301
        }
    }

}
