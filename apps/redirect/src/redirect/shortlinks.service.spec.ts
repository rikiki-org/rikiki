import { Test, TestingModule } from '@nestjs/testing';
import { ShortlinksService } from './shortlinks.service';
import { HttpModule } from '@nestjs/common';

describe('ShortlinksService', () => {
  let service: ShortlinksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ShortlinksService],
      imports: [HttpModule],
    }).compile();

    service = module.get<ShortlinksService>(ShortlinksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
