import { Test, TestingModule } from '@nestjs/testing';
import { ShortlinkService } from './shortlink.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ShortlinkEntity } from './ShortlinkEntity';
import { Repository } from 'typeorm';
import { BadRequestException } from '@nestjs/common';

describe('ShortlinkService', () => {
  let service: ShortlinkService;
  let repository: Repository<ShortlinkEntity>;
  const shortLinkFixtures = [new ShortlinkEntity(), new ShortlinkEntity()];

  beforeEach(async () => {
    const mockRepository = {
      findOne: jest.fn(),
      find: jest.fn(),
      save: jest.fn(),
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [ShortlinkService,
        {
          provide: getRepositoryToken(ShortlinkEntity),
          useValue: mockRepository
        }],
    }).compile();

    service = module.get<ShortlinkService>(ShortlinkService);
    repository = module.get<Repository<ShortlinkEntity>>(getRepositoryToken(ShortlinkEntity))
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('getAll should return what repository return', async () => {
    const findSpy = jest.spyOn(repository, 'find').mockResolvedValue(shortLinkFixtures);
    expect(await service.getAll()).toEqual(shortLinkFixtures);
    expect(findSpy).toHaveBeenCalledTimes(1);
  });

  it('getBySlug should return what repository return', async () => {
    const findOneSpy = jest.spyOn(repository, 'findOne').mockResolvedValue(shortLinkFixtures[0]);
    expect(await service.getBySlug('foo')).toEqual(shortLinkFixtures[0]);
    expect(findOneSpy).toHaveBeenCalledTimes(1);
    expect(findOneSpy).toHaveBeenCalledWith('foo');
  });

  it('add should throw BadRequest if slug not avaialble', async () => {
    jest.spyOn(service, 'slugIsAvailable').mockResolvedValue(false);
    expect(service.add(shortLinkFixtures[0])).rejects.toEqual(new BadRequestException('Already exist'));
  });

  it('add should save if slug avaialble', async () => {
    jest.spyOn(service, 'slugIsAvailable').mockResolvedValue(true)
    const saveSpy = jest.spyOn(repository, 'save').mockImplementation(input => Promise.resolve(input) as unknown as Promise<ShortlinkEntity>);
    expect(await service.add(shortLinkFixtures[0])).toEqual(shortLinkFixtures[0]);
    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(saveSpy).toHaveBeenCalledWith(shortLinkFixtures[0]);
  });

  it('getRandomSlug should generate slug', async () => {
    jest.spyOn(service, 'slugIsAvailable').mockResolvedValue(true);
    expect(await service.getRandomSlug()).toHaveLength(5);
    expect(await service.getRandomSlug(8)).toHaveLength(8);
  });

  it('getRandomSlug should retry until available slug', async () => {
    const slugIsAvailableSpy = jest.spyOn(service, 'slugIsAvailable')
      .mockResolvedValueOnce(false)
      .mockResolvedValueOnce(false)
      .mockResolvedValueOnce(true)

    expect(await service.getRandomSlug()).toHaveLength(5);
    expect(slugIsAvailableSpy).toHaveBeenCalledTimes(3)
  });

  it('slugIsAvaialble should return true if getBySlug is undefined', async () => {
    jest.spyOn(service, 'getBySlug').mockResolvedValue(undefined)
    expect(await service.slugIsAvailable('foo')).toEqual(true);
    jest.spyOn(service, 'getBySlug').mockResolvedValue(shortLinkFixtures[0]);
    expect(await service.slugIsAvailable('foo')).toEqual(false);
  });

});
