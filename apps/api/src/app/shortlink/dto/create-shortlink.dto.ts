import { IsUrl, IsAlphanumeric, Length, IsOptional } from 'class-validator'
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IShortlinkCreateDto } from '@rikiki/models'

export class CreateShortlinkDto implements IShortlinkCreateDto {
    @ApiPropertyOptional()
    @Length(4, 10)
    @IsAlphanumeric()
    @IsOptional()
    slug?: string
    
    @ApiProperty()
    @IsUrl()
    longUrl: string
}