import { Test, TestingModule } from '@nestjs/testing';
import { ShortlinkController } from './shortlink.controller';
import { ShortlinkService } from './shortlink.service';
import { NotFoundException } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ShortlinkEntity } from './ShortlinkEntity';
import { CreateShortlinkDto } from './dto/create-shortlink.dto';

describe('Shortlink Controller', () => {
  let controller: ShortlinkController;
  let service: ShortlinkService;

  beforeEach(async () => {

    const mockRepository = jest.fn().mockReturnValue({
      findOne: jest.fn(),
      find: jest.fn(),
      save: jest.fn(),
    })

    const module: TestingModule = await Test.createTestingModule({
      controllers: [ShortlinkController],
      providers: [ShortlinkService,
      {
        provide: getRepositoryToken(ShortlinkEntity),
        useValue: mockRepository
      }]
    }).compile();

    controller = module.get<ShortlinkController>(ShortlinkController);
    service = module.get<ShortlinkService>(ShortlinkService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('getAll should return what the service getAll return', async () => {
    const result = [{slug: 'test', longUrl: 'http://google.com'}];
    jest.spyOn(service, 'getAll').mockResolvedValue(result);
    expect(await controller.getAll()).toBe(result);
  })

  it('getBySlug should return what the service getBySlug return', async () => {
    const result = {slug: 'test', longUrl: 'http://google.com'};
    jest.spyOn(service, 'getBySlug').mockResolvedValue(result);
    expect(await controller.getBySlug(result.slug)).toBe(result);
  })

  it('getBySlug should throw 404 when the service getBySlug return not found', () => {
    jest.spyOn(service, 'getBySlug').mockResolvedValue(undefined);
    expect(controller.getBySlug("test")).rejects.toEqual(new NotFoundException());
  })


  it('add without slug should ask service to generate one', async () => {
    const payload: CreateShortlinkDto = {longUrl: "https://foo.bar"}
    const slug = 'abcd'

    const result = {...payload, slug}
    
    jest.spyOn(service, 'getRandomSlug').mockImplementation(() => Promise.resolve(slug));
    const addSpy = jest.spyOn(service, 'add').mockImplementation(async input => input);

    expect(await controller.add(payload)).toEqual(result)
    expect(addSpy).toHaveBeenCalledWith(result)
  })

  it('add with slug should send to the service', async () => {
    const payload: CreateShortlinkDto = {longUrl: "https://foo.bar", slug: 'abcd'}

    const getRandomSlugSpy = jest.spyOn(service, 'getRandomSlug').mockImplementation();
    const addSpy = jest.spyOn(service, 'add').mockImplementation(async input => input);

    expect(await controller.add(payload)).toEqual(payload)
    expect(addSpy).toHaveBeenCalledWith(payload)
    expect(getRandomSlugSpy).toHaveBeenCalledTimes(0)
  })

});
