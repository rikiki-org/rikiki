import { Injectable, BadRequestException } from '@nestjs/common';
import { ShortlinkEntity } from './ShortlinkEntity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ShortlinkService {

    constructor(
        @InjectRepository(ShortlinkEntity) 
        private shortlinkRepository: Repository<ShortlinkEntity>
    ){}

    async getAll(): Promise<ShortlinkEntity[]> {
        return this.shortlinkRepository.find()
    }

    async getBySlug(slug: string): Promise<ShortlinkEntity | undefined> {
        return this.shortlinkRepository.findOne(slug)
    }

    async add(shortlink: ShortlinkEntity): Promise<ShortlinkEntity> {
        if (! await this.slugIsAvailable(shortlink.slug)) {
            throw new BadRequestException('Already exist')
        }
        return this.shortlinkRepository.save(shortlink)
    }

    async getRandomSlug(lenght = 5 ): Promise<string>{
        let slug;
        do {
            slug = Math.random().toString(36).substr(2, lenght);
        } while (!await this.slugIsAvailable(slug));
        return slug;
    }

    async slugIsAvailable(slug: string): Promise<boolean> {        
        return await this.getBySlug(slug) === undefined
    }

}
