import { ApiProperty } from '@nestjs/swagger'
import { Entity, PrimaryColumn, Column } from 'typeorm'
import { Shortlink } from '@rikiki/models';

@Entity()
export class ShortlinkEntity implements Shortlink {

    @ApiProperty()
    @PrimaryColumn({unique: true})
    slug: string

    @ApiProperty()
    @Column()
    longUrl: string
}