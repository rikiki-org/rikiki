import { Module } from '@nestjs/common';
import { ShortlinkController } from './shortlink.controller';
import { ShortlinkService } from './shortlink.service';
import { ShortlinkEntity } from './ShortlinkEntity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ShortlinkEntity])],
  controllers: [ShortlinkController],
  providers: [ShortlinkService],
  exports: [ShortlinkService]
})
export class ShortlinkModule {}
