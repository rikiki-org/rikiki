import { Controller, Get, Post, Param, NotFoundException, Body } from '@nestjs/common';
import { ShortlinkService } from './shortlink.service';
import { ShortlinkEntity } from './ShortlinkEntity';
import { CreateShortlinkDto } from './dto/create-shortlink.dto';
import { ApiTags, ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';

@ApiTags("shortlinks")
@Controller('shortlinks')
export class ShortlinkController {
    
    constructor(private shortlinkService: ShortlinkService) {}

    @Get()
    @ApiOkResponse({type: [ShortlinkEntity]})
    async getAll(): Promise<ShortlinkEntity []> {
        return this.shortlinkService.getAll();
    }

    @Get(':slug')
    @ApiOkResponse({type: ShortlinkEntity})
    async getBySlug(@Param('slug') slug: string): Promise<ShortlinkEntity>  {
        const shortlink =  await this.shortlinkService.getBySlug(slug);
        if (!shortlink) {
            throw new NotFoundException();
        }
        return shortlink;
    }

    @Post()
    @ApiCreatedResponse({type: ShortlinkEntity})
    async add(@Body() createShortlinkDto: CreateShortlinkDto): Promise<ShortlinkEntity> {

        const newShortlink = new ShortlinkEntity();
        newShortlink.longUrl = createShortlinkDto.longUrl;

        newShortlink.slug = createShortlinkDto.slug ? createShortlinkDto.slug : await this.shortlinkService.getRandomSlug(5)

        return await this.shortlinkService.add(newShortlink);
    }
}
