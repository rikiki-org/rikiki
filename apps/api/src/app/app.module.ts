import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ShortlinkModule } from './shortlink/shortlink.module';
import { Connection } from 'typeorm';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';

@Module({
  imports: [ShortlinkModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useClass: ConfigService
    }),
    ConfigModule,
  ],
  providers: [],

})
export class AppModule {
  constructor(private connection: Connection) {}
}
