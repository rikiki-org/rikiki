import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from './config.service';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

describe('ConfigService', () => {
  let service: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConfigService],
    }).compile();

    service = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });


  it('should return the default values', () => {
    const config = service.createTypeOrmOptions() as PostgresConnectionOptions;
    expect(config.host).toEqual("localhost");
    expect(config.port).toEqual(5432);
  })

  it('process env should overide default values', () => {

    const oldProcessEnv = process.env;

    process.env.DATABASE_HOST = "postgres";
    process.env.DATABASE_PORT = "420";

    const config = service.createTypeOrmOptions() as PostgresConnectionOptions;

    expect(config.host).toEqual("postgres");
    expect(config.port).toEqual(420);

    process.env = oldProcessEnv;
  })
});
