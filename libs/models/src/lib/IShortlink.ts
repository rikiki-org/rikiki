export interface IShortlink {
    slug: string
    longUrl: string
}