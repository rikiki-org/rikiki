export interface IShortlinkCreateDto {
    slug?: string
    longUrl: string
}