import { IShortlink } from '..'

export class Shortlink implements IShortlink {

    slug: string

    longUrl: string
    
}
